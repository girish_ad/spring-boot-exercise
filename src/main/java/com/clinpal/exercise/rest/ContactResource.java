package com.clinpal.exercise.rest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.clinpal.exercise.service.contact.ContactService;
import com.clinpal.exercise.service.contact.domain.Contact;

import java.util.List;

/**
 * The REST API for the Contact Service
 */
@RestController
@RequestMapping("/api")
public class ContactResource {

    private final ContactService contactService;

    @Autowired
    public ContactResource(final ContactService contactService) {
        this.contactService = contactService;
    }

    /**
     * REST API to obtain all contacts
     *
     * @return all of the contacts as JSON
     */
    @GetMapping(value = "/contacts", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<Contact>> getContacts() {
        return ResponseEntity.ok(contactService.findAll());
    }
}
