package com.clinpal.exercise.service.contact.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.clinpal.exercise.service.contact.ContactService;
import com.clinpal.exercise.service.contact.domain.Contact;

import java.util.List;

/**
 * The concrete implementation of the contact service
 */
@Service
class ContactServiceImpl implements ContactService {

    /** The repository DOA layer */
    private final ContactRepository repository;

    @Autowired
    private ContactServiceImpl(final ContactRepository repository) {
        this.repository = repository;
    }

    @Override public Contact save(final Contact contact) {
        return repository.save(contact);
    }

    @Override public List<Contact> findContactBySurnameLike(final String surname) {
        return repository.findContactBySurnameLike("%" + surname + "%");
    }

    @Override public List<Contact> findAll() {
        return repository.findAll();
    }
}
